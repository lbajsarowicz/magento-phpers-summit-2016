<?php
/**
 * CategoryAllowed category controller
 *
 * @package Bajsarowicz_CategoryAllowed
 */

require_once 'Mage/Catalog/controllers/CategoryController.php';

class Bajsarowicz_CategoryAllowed_CategoryController extends Mage_Catalog_CategoryController
{
    const CATEGORY_ALLOWED_NOTICE = 'Category is available for registered users only';

    function _initCatagory()
    {
        $category = parent::_initCatagory();

        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        $isCategoryAllowed = $category->getData('is_category_allowed');

        if (!$session->isLoggedIn() && !$isCategoryAllowed) {
            $session->addNotice( _(self::CATEGORY_ALLOWED_NOTICE) );
            $this->_redirect('customer/account/login');
        }

        return $category;
    }
}