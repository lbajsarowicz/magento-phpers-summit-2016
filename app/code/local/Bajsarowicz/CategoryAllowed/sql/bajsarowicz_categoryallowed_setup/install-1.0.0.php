<?php
/**
 * @var Mage_Eav_Model_Entity_Setup $this
 */

$this->startSetup();
$this->addAttribute('catalog_category', 'is_category_allowed', array(
    'group' => 'General',
    'type' => 'int',
    'backend' => '',
    'frontend_input' => '',
    'frontend' => '',
    'label' => 'Is category allowed?',
    'input' => 'select',
    'default' => 0,
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'frontend_class' => '',
    'required' => false,
    'user_defined' => true,
    'position' => 666
));

$this->endSetup();